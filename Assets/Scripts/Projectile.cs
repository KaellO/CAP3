using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    GameObject shooter;
    float Damage;
    LayerMask targetLayer;

    public void InitProjectile(GameObject pshooter, float pdamage, LayerMask ptargetLayer)
    {
        shooter = pshooter;
        Damage = pdamage;
        targetLayer = ptargetLayer;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == targetLayer)
        {
            if (collision.gameObject.TryGetComponent<HealthComponent>(out HealthComponent health)) 
            {
                health.ReduceHealth(Damage, shooter);
            }
            GameObject.Destroy(this.gameObject);
        }

    }
}
