using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    [SerializeField] GameObject chargeBar;
    [SerializeField] Image barImage;

    private void Start()
    {
        GetComponentInParent<PlayerController>().OnCharging += UpdateBar;
    }
    void UpdateBar(float value, float maxValue)
    {
        if (value == 0)
        {
            chargeBar.SetActive(false);
            return;
        }

        chargeBar.SetActive(true);
        barImage.fillAmount = value / maxValue;
    }
}
