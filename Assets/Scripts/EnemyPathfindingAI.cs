using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Pathfinding;

public class EnemyPathfindingAI : MonoBehaviour
{
    [Header("Pahtfinding")]
    public Transform target;
    public float activateDistance = 50f;
    public float pathUpdateSeconds = 0.5f;
    public float stoppingDistance = 2f;

    [Header("Physics")]
    public float speed = 2f;
    public float nextWaypointDistance = 3f;
    public float jumpNodeHeightRequirement = 0.8f;
    public float jumpNodeHeightLimit = 2f;
    public float jumpForce = 0.3f;

    [Header("Custom Behavior")]
    public bool canMove = true;
    public bool jumpEnabled = true;
    public bool directionLookEnabled = true;

    Path path;
    int currentWaypoint = 0;
    Seeker seeker;
    Rigidbody2D rb2d;
    SpriteRenderer spr;
    GroundedCheck groundCheck;

    void Awake()
    {
        seeker = GetComponent<Seeker>();
        rb2d = GetComponent<Rigidbody2D>();
        spr = GetComponentInChildren<SpriteRenderer>();
        groundCheck = GetComponent<GroundedCheck>();
    }

    private void Start()
    {
        InvokeRepeating("UpdatePath", 0f, pathUpdateSeconds);
    }

    void UpdatePath()
    {
        if (canMove && TargetInDistance() && seeker.IsDone())
        {
            seeker.StartPath(rb2d.position, target.position, OnPathComplete);
        }
    }

    public void PathFollow()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            return;
        }


        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb2d.position).normalized;
        Vector2 force = direction * speed * Time.fixedDeltaTime;

        transform.Translate(new Vector2(direction.x, direction.y) * speed * Time.deltaTime);
        Debug.Log(direction);
        //rb2d.AddForce(force, ForceMode2D.Force);

        if (jumpEnabled && groundCheck.IsGrounded())
        {
            if (direction.y > jumpNodeHeightRequirement && jumpNodeHeightLimit > direction.y)
            {
                rb2d.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }
        }

        float distance = Vector2.Distance(rb2d.position, path.vectorPath[currentWaypoint]);
        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }
    }

    public void Flee(float fleeSpeed)
    {
        Vector2 Dir = (transform.position - target.transform.position).normalized;

        transform.Translate(Dir * fleeSpeed * Time.deltaTime);
    }

    public void ChangeLookDirection()
    {
        Vector2 Dir = (transform.position - target.transform.position).normalized;

        if (directionLookEnabled)
        {
            if (Dir.x < 0)
            {
                spr.flipX = false;
            }
            else if (Dir.x > 0)
            {
                spr.flipX = true;
            }
        }
    }

    public void FleeLookDirection()
    {
        Vector2 Dir = (transform.position - target.transform.position).normalized;

        if (directionLookEnabled)
        {
            if (Dir.x > 0)
            {
                spr.flipX = false;
            }
            else if (Dir.x < 0)
            {
                spr.flipX = true;
            }
        }

    }

    public bool TargetInStoppingDistance()
    {
        return Vector2.Distance(transform.position, target.transform.position) < stoppingDistance;
    }

    public bool TargetInDistance()
    {
        return Vector2.Distance(transform.position, target.transform.position) < activateDistance;
    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(transform.position, stoppingDistance);
    }

}
