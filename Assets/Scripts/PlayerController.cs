using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Movement movement;
    AnimationController animController;
    Animator anim;
    Attack attack;
    HealthComponent health;
    SpriteRenderer spr;
    FireProjectile fireProjectile;
    GroundedCheck groundCheck;

    Vector2 moveInput;
    Vector2 mousePos;

    Rigidbody2D rb2d;
    Camera cam;
    public Vector3 ArrowDir { get; private set; }

    //Temporary variables
    float DashPower = 180f;
    float DashIFrames = 0.15f;
    float DashCooldown = 0.5f;
    float DashCooldownTimer = 0f;
    float DashDir = 1f;
    bool canDash = true;

    //Temporary again, casting
    float CastTime = 1f;
    float CurrentCastTimer = 0;
    bool casting = false;
    public Action<float, float> OnCharging;

    enum MouseDir
    {
        Up = 270,
        Down = 90,
        Left = 0,
        Right = 180
    }

    [SerializeField] GameObject Arrow;
    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponent<Movement>();
        attack = GetComponent<Attack>();
        rb2d = GetComponent<Rigidbody2D>();
        animController = GetComponentInChildren<AnimationController>();
        anim = GetComponentInChildren<Animator>();
        spr = GetComponentInChildren<SpriteRenderer>();
        health = GetComponent<HealthComponent>();
        fireProjectile = GetComponent<FireProjectile>();
        groundCheck = GetComponent<GroundedCheck>();

        cam = Camera.main;

        health.OnDamage += OnDamage;
        animController.OnSpawnAttack += SpawnHurtBox;
    }


    // Update is called once per frame
    void Update()
    {
        moveInput.x = Input.GetAxisRaw("Horizontal");
        anim.SetFloat("Speed", moveInput.sqrMagnitude);
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        if (attack.currAttackTime <= 0 && !casting)
        {
            FlipSprite();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && DashCooldownTimer <= 0 
            && attack.currAttackTime <= 0 && canDash && !casting)
        {
            Dash();
        }

        if (Input.GetMouseButtonDown(0) && attack.currAttackTime <= 0 && DashCooldownTimer <= 0 && !casting)
        {
            rb2d.velocity = new Vector3(0, rb2d.velocity.y);
            attack.StartAttackDelay();
            animController.ChangeAnimation(AnimationController.Animation.AttackRight);
        }

        ShootProjectile();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            movement.Jump();
        }

        if (DashCooldownTimer > 0)
        {
            DashCooldownTimer -= Time.deltaTime;
        }
        else if (groundCheck.IsGrounded())
            canDash = true;

        if (CurrentCastTimer < CastTime)
        {
            CurrentCastTimer += Time.deltaTime;
            CurrentCastTimer = Mathf.Clamp(CurrentCastTimer, 0, CastTime);
        }
    }
    private void FixedUpdate()
    {
        if (attack.currAttackTime <= 0 && !casting)
        {
            movement.Move(moveInput.x);
        }
    }

    public void FlipSprite()
    {
        if (moveInput.x == -1)
        {
            spr.flipX = true;
            DashDir = -1;
        }
        else if (moveInput.x == 1)
        {
            spr.flipX = false;
            DashDir = 1;
        }
    }

    void ShootProjectile()
    {
        //Vector2 lookDir = Vector2.zero;

        Vector2 lookDir = mousePos - new Vector2(transform.position.x, transform.position.y);

        if (Input.GetMouseButtonDown(1))
        {
            Arrow.SetActive(true);
            casting = true;
            rb2d.gravityScale = 0;
            CurrentCastTimer = 0;
        }

        if (Input.GetMouseButton(1))
        {
            //cast
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
            rb2d.velocity = Vector3.zero;

            OnCharging?.Invoke(CurrentCastTimer, CastTime);
            Arrow.transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        if (Input.GetMouseButtonUp(1))
        {
            Arrow.SetActive(false);
            casting = false;
            rb2d.gravityScale = 1;

            if (CurrentCastTimer < CastTime)
            {
                CurrentCastTimer = 0;
                OnCharging?.Invoke(CurrentCastTimer, CastTime);
                return;
            }

            CurrentCastTimer = 0;
            OnCharging?.Invoke(CurrentCastTimer, CastTime);
            //float angleDir = angle + 270;
            ArrowDir = lookDir.normalized;
            fireProjectile.ShootProjectile(ArrowDir, 1, 6);
        }
    }

    void Dash()
    {
        DashCooldownTimer = DashCooldown;
        Coroutine IFrame = null;

        if (IFrame == null)
            IFrame = StartCoroutine(IFrames());
    }

    void SpawnHurtBox()
    {
        if (spr.flipX == true)
            attack.SpawnAttackSphere(Attack.AttackDir.Left);
        else
            attack.SpawnAttackSphere(Attack.AttackDir.Right);
    }

    IEnumerator IFrames()
    {
        Color sprColor = spr.color;
        canDash = false;
        Physics2D.IgnoreLayerCollision(7, 9, true);
        Physics2D.IgnoreLayerCollision(6, 9, true);
        Physics2D.IgnoreLayerCollision(9, 9, true);

  
        health.Invulnerable = true;
        spr.color = Color.blue;

        rb2d.AddForce(new Vector2(DashDir, 0) * DashPower, ForceMode2D.Impulse);

        yield return new WaitForSeconds(DashIFrames);

        Physics2D.IgnoreLayerCollision(7, 9, false);
        Physics2D.IgnoreLayerCollision(6, 9, false);
        Physics2D.IgnoreLayerCollision(9, 9, false);

        spr.color = sprColor;
        health.Invulnerable = false;
    }

    void OnDamage(float damage)
    {
        StartCoroutine(Flash());
    }

    IEnumerator Flash()
    {
        Color sprColor = spr.color;
        health.Invulnerable = true;
        for (int i = 0; i < 3; i++)
        {
            spr.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            spr.color = sprColor;
            yield return new WaitForSeconds(0.1f);
            yield return null;
        }
        health.Invulnerable = false;
    }

    //Deprecated
    //void ChangeAttackAngle(int angleDir)
    //{
    //    switch (angleDir)
    //    {
    //        case int n when (n < 270 + 45 && n > 270 - 45):
    //            animController.ChangeAnimation(AnimationController.Animation.AttackUp);
    //            attack.SpawnAttackSphere(Attack.AttackDir.Top);
    //            break;

    //        case int n when (n < 90 + 45 && n > 90 - 45):
    //            animController.ChangeAnimation(AnimationController.Animation.AttackDown);
    //            attack.SpawnAttackSphere(Attack.AttackDir.Bot);
    //            break;

    //        case int n when (n < 180 + 45 && n > 180 - 45):
    //            animController.ChangeAnimation(AnimationController.Animation.AttackRight);
    //            attack.SpawnAttackSphere(Attack.AttackDir.Right);
    //            break;

    //        case int n when (n > 45 || n < 315 + 45):
    //            animController.ChangeAnimation(AnimationController.Animation.AttackLeft);
    //            attack.SpawnAttackSphere(Attack.AttackDir.Left);
    //            break;
    //    }
    //}
}
