using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    protected HealthComponent healthComponent;
    protected AnimationController animController;
    protected Animator animator;
    protected Collider2D col;
    protected Rigidbody2D rb2d;
    protected Attack attack;
    protected EnemyPathfindingAI pathfindingAI;
    protected SpriteRenderer spr;
    protected GroundedCheck groundCheck;

    [SerializeField] GameObject ColBlocker;
    //Knockback
    Vector2 dir = Vector2.zero;
    protected float knockback = 0.5f;

    protected bool dead = false;

    protected void Awake()
    {
        healthComponent = GetComponent<HealthComponent>();
        animator = GetComponentInChildren<Animator>();
        col = GetComponent<Collider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        attack = GetComponent<Attack>();
        animController = GetComponentInChildren<AnimationController>();
        pathfindingAI = GetComponent<EnemyPathfindingAI>();
        spr = GetComponentInChildren<SpriteRenderer>();
        groundCheck = GetComponent<GroundedCheck>();
    }
    // Start is called before the first frame update
    protected virtual void Start()
    {
        animController.OnSpawnAttack += SpawnAttackType;
        healthComponent.OnDamage += OnDamage;
        healthComponent.OnZeroHealth += OnDeath;
    }

    protected virtual void SpawnAttackType()
    {
        if (spr.flipX == false)
        {
            attack.SpawnAttackSphere(Attack.AttackDir.Right);
        }
        else
            attack.SpawnAttackSphere(Attack.AttackDir.Left);
    }

    protected virtual void FixedUpdate()
    {
        if (dead)
        {
            if (groundCheck.IsGrounded())
            {
                rb2d.velocity = Vector2.zero;
                rb2d.gravityScale = 0;
                col.enabled = false;
                ColBlocker.SetActive(false);
            }
            else
            {
                rb2d.gravityScale = 1;
            }
            return;
        }


        if (pathfindingAI.TargetInStoppingDistance() && attack.currAttackTime <= 0)
        {
            pathfindingAI.ChangeLookDirection();
            OnAttack();
            return;
        }

        if (pathfindingAI.TargetInDistance() && pathfindingAI.canMove && attack.currAttackTime <= 0)
        {
            pathfindingAI.ChangeLookDirection();
            pathfindingAI.PathFollow();
            animController.ChangeAnimation(AnimationController.Animation.Walk);
        }
    }

    protected void OnAttack()
    {
        animController.ChangeAnimation(AnimationController.Animation.AttackUp);
        attack.StartAttackDelay();
    }

    protected void OnDamage(float damage)
    {
        animController.ChangeAnimation(AnimationController.Animation.Hurt);

        KnockBack();
    }

    protected void OnDeath()
    {
        dead = true;
        rb2d.velocity = Vector2.zero;
        pathfindingAI.canMove = false;
        animController.ChangeAnimation(AnimationController.Animation.Death);
    }

    protected void KnockBack()
    {
        //dir = (this.transform.position - healthComponent.DamagerData.GetComponent<PlayerController>().ArrowPos).normalized;
        dir = healthComponent.DamagerData.GetComponent<PlayerController>().ArrowDir * knockback;
        dir += (Vector2)transform.position;
        rb2d.MovePosition(dir);

        Coroutine KBack = null;
        if (KBack == null)
        {
            KBack = StartCoroutine(KnockBackTime());
        }
    }

    IEnumerator KnockBackTime()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
