using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] float maxHP;
    [SerializeField] float currentHp;

    public Action<float> OnDamage;
    public Action<float> ReduceHealthUI;
    public Action<float> OnRegen;
    public Action<float> OnSetHP;
    public Action OnZeroHealth;
    public Action InvulHit;
    public GameObject DamagerData { get; private set; }
    public bool Invulnerable = false;

    public float MaxHP { get { return maxHP; } }

    public void InitData(float pMaxHp)
    {
        maxHP = pMaxHp;
        currentHp = maxHP;
    }

    public void ReduceHealth(float value, GameObject damager)
    {
        DamagerData = damager;

        if (Invulnerable)
        {
            InvulHit?.Invoke();
            return;
        }

        currentHp -= Mathf.Clamp(value, 0, maxHP);
        ReduceHealthUI?.Invoke(value);

        //StartCoroutine(DamageAnim());
        if (currentHp <= 0)
        {
            OnZeroHealth?.Invoke();
        }
        else 
        {
            OnDamage?.Invoke(value);
        }
    }

    public void AddHealth(float value)
    {
        currentHp += Mathf.Clamp(value, 0, maxHP);
        OnRegen?.Invoke(value);
    }

    public void ResetHealth()
    {
        currentHp = maxHP;
        OnSetHP?.Invoke(currentHp);
    }
}
