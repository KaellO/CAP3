using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireProjectile : MonoBehaviour
{
    [SerializeField] GameObject projectilePrefab;

    [SerializeField] float bulletSpeed;
    public void ShootProjectile(Vector3 dir, float damage, LayerMask targetLayer)
    {
        GameObject spawnedProjectile = GameObject.Instantiate(projectilePrefab, this.transform);
        Rigidbody2D projectileRb = spawnedProjectile.GetComponent<Rigidbody2D>();
        Projectile projectileComp = spawnedProjectile.GetComponent<Projectile>();

        projectileRb.AddForce(dir * bulletSpeed, ForceMode2D.Impulse);
        projectileComp.InitProjectile(this.gameObject, damage, targetLayer);
    }
}
