using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator animator;
    public Action OnSpawnAttack;
    public enum Animation
    {
        AttackUp,
        AttackDown,
        AttackLeft,
        AttackRight,
        Hurt,
        Walk,
        Death,
        Jump
    }
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void ChangeAnimation(Animation name)
    {
        animator.Play(name.ToString());
    }

    public void AttackEvent()
    {
        OnSpawnAttack?.Invoke();
    }
}
