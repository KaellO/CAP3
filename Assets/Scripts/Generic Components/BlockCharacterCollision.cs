using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCharacterCollision : MonoBehaviour
{
    public CapsuleCollider2D characterCollider;
    public CapsuleCollider2D collisionBlocker;
    void Start()
    {
        Physics2D.IgnoreCollision(characterCollider, collisionBlocker, true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
