using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody2D rb2d;
    Animator anim;
    GroundedCheck groundedComponent;

    [Header("Jump Parameters")]
    public float JumpForce = 5f;
    [SerializeField] float fallMultiplier = 2.5f;
    [SerializeField] float lowJumpMultiplier = 2f;
    [SerializeField] float jumpCoyoteTime;
    [SerializeField] float jumpBufferTime;

    bool canCoyote = false;
    bool isJumping = false;

    float lastGroundedTime = 1;
    float jumpBufferTimer;

    [Header("Movement")]
    public float MoveSpeed = 5f;
    [SerializeField] float acceleration = 2f;
    [SerializeField] float deceleration = 2f;
    [SerializeField] float velPower = 0.9f;
    [SerializeField] float frictionAmount = 0.2f;

    bool canJumpBuffer = false;
    // Start is called before the first frame update
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        groundedComponent = GetComponent<GroundedCheck>();
    }

    private void Update()
    {
        if (lastGroundedTime > 0)
            lastGroundedTime -= Time.deltaTime;

        jumpBufferTimer -= Time.deltaTime;

        if (lastGroundedTime > 0 && !isJumping)
            canCoyote = true;
        else
            canCoyote = false;

        if (rb2d.velocity.y == 0 && !canCoyote)
        {
            isJumping = false;
        }

        if (jumpBufferTimer > 0 && canJumpBuffer)
        {
            isJumping = true;
            rb2d.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
            jumpBufferTimer = 0;
        }
    }

    private void FixedUpdate()
    {
        if (groundedComponent.IsGrounded())
        {
            lastGroundedTime = jumpCoyoteTime;
            canJumpBuffer = true;
        }
        else
        {
            canJumpBuffer = false;
        }

        anim.SetBool("isGrounded", groundedComponent.IsGrounded());

        if (rb2d.velocity.y < 0)
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb2d.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    public void Move(float moveInput)
    {
        float targetSpeed = moveInput * MoveSpeed;
        float speedDif = targetSpeed - rb2d.velocity.x;
        float accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? acceleration : deceleration;
        float movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, velPower) * Mathf.Sign(speedDif);

        rb2d.AddForce(movement * Vector2.right);

        //Friction
        if (lastGroundedTime > 0 && moveInput == 0)
        {
            float amount = Mathf.Min(Mathf.Abs(rb2d.velocity.x), Mathf.Abs(frictionAmount));
            amount *= Mathf.Sign(rb2d.velocity.x);
            rb2d.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
        }
    }

    public void Jump()
    {
        jumpBufferTimer = jumpBufferTime;

        if (!canCoyote && !canJumpBuffer)
            return;

        isJumping = true;
        rb2d.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
        jumpBufferTimer = 0;
    }
}
