using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public LayerMask targetLayer;
    public float attackRadius;
    public Transform attackPoint;
    AnimationController anim;

    [SerializeField] float attackPointY;
    Vector2 attackPointInitalPos;

    public float damage;
    public float attackDelay;
    public float currAttackTime { get; private set; }

    private void Awake()
    {
        anim = GetComponentInChildren<AnimationController>();
    }

    private void Start()
    {
        attackPointInitalPos = attackPoint.transform.localPosition;
        currAttackTime = 0;
    }

    public enum AttackDir
    {
        Top = 1,
        Bot = -1,
        Left = -2,
        Right = 2
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }

    public void SpawnAttackSphere(AttackDir dir)
    {
        ChangeAttackPointOrientation(dir);

        Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius, targetLayer);

        foreach (Collider2D enemy in hit)
        {
            if (enemy.TryGetComponent(out HealthComponent healthComp))
            {
                healthComp.ReduceHealth(damage, this.gameObject);
            }
        }
    }

    void ChangeAttackPointOrientation(AttackDir dir)
    {
        switch (dir)
        {
            case AttackDir.Top:
            case AttackDir.Bot:
                attackPoint.transform.localPosition = new Vector2(0, attackPointY * Mathf.Sign((int)dir));
                break;

            case AttackDir.Left:
            case AttackDir.Right:
                attackPoint.transform.localPosition = new Vector2(attackPointInitalPos.x * Mathf.Sign((int)dir), attackPointInitalPos.y);
                break;
        }
    }

    public void StartAttackDelay()
    {
        currAttackTime = attackDelay;
    }

    private void Update()
    {
        if (currAttackTime > 0)
        {
            currAttackTime -= Time.deltaTime;
        }
    }
}
