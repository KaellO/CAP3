using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingRangedAI : EnemyController
{
    [SerializeField] float fleeDistance;
    [SerializeField] float fleeSpeed;
    FireProjectile fireProjectile;
    protected override void Start()
    {
        base.Start();
        fireProjectile = GetComponent<FireProjectile>();
    }

    bool TargetTooClose()
    {
        return Vector2.Distance(transform.position, pathfindingAI.target.transform.position) < fleeDistance;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (TargetTooClose() && pathfindingAI.canMove)
        {
            pathfindingAI.ChangeLookDirection();
            pathfindingAI.Flee(fleeSpeed);
            animController.ChangeAnimation(AnimationController.Animation.AttackUp);
        }
    }
    protected override void SpawnAttackType()
    {
        Vector2 Dir = (pathfindingAI.target.position - this.transform.position).normalized;
        fireProjectile.ShootProjectile(Dir, attack.damage, 7);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;

        Gizmos.DrawWireSphere(transform.position, fleeDistance);
    }
}
